package com.example.tpimagemcapron;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

public class ImageManager {
    Bitmap srcBitmap;

    /**
     * Image flipping :
     * - Horizontally
     * - Vertically
     * - Clockwise
     * - Anticlockwise
     */
    private Bitmap flipImage(String type){
        Matrix matrix = new Matrix();

        //Based on the type, the matrix should scale differently
        switch (type) {
            case "h":
                matrix.preScale(-1.0f, 1.0f);
                break;
            case "v":
                matrix.preScale(1.0f, -1.0f);
                break;
            case "cw":
                matrix.preRotate(90);
                break;
            case "acw":
                matrix.preRotate(-90);
        }

        //Creating the new bitmap image based on the new given matrix
        return Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
    }

    public Bitmap mirrorImageHorizontal(){
        return flipImage("h");
    }

    public Bitmap mirrorImageVertical(){
        return flipImage("v");
    }

    public Bitmap clockwise(){
        return flipImage("cw");
    }

    public Bitmap anticlockwise(){
        return flipImage("acw");
    }

    /** End flipping image **/

    /**
     * Inverting colors is kind of slow and can provock some latency
     * because unlike Matrix API this one is not optimized
     */
    public Bitmap invertColors() {
        // copy the bitmap
        Bitmap bmOut = Bitmap.createBitmap(srcBitmap.getWidth(), srcBitmap.getHeight(), srcBitmap.getConfig());
        // color and pixel
        int A, R, G, B, pixelColor;
        // image size
        int height = srcBitmap.getHeight();
        int width = srcBitmap.getWidth();

        // check every pixel
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                // pixel on the given pos
                pixelColor = srcBitmap.getPixel(x, y);
                // we keep the Alpha for next moves
                A = Color.alpha(pixelColor);
                // inverting all
                R = 255 - Color.red(pixelColor);
                G = 255 - Color.green(pixelColor);
                B = 255 - Color.blue(pixelColor);
                // we set the new bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        return bmOut;
    }

    public Bitmap toGrayLevel(int grayLevel){
        // copy the bitmap
        Bitmap bmOut = Bitmap.createBitmap(srcBitmap.getWidth(), srcBitmap.getHeight(), srcBitmap.getConfig());
        // color and pixel
        int A, R, G, B, pixelColor;
        // image size
        int height = srcBitmap.getHeight();
        int width = srcBitmap.getWidth();

        // check every pixel
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                // pixel on the given pos
                pixelColor = srcBitmap.getPixel(x, y);
                // we keep the Alpha for next moves
                A = Color.alpha(pixelColor);
                R = Color.red(pixelColor);
                G = Color.green(pixelColor);
                B = Color.blue(pixelColor);
                int global = 0;
                //graylevel test
                //based on the button clicked
                switch (grayLevel) {
                    case 1:
                        global = (R + G + B) / 3;
                        break;
                    case 2:
                        global = (Math.max(R, Math.max(G, B)) + Math.min(R, Math.min(G, B))) / 2;
                        break;
                    case 3:
                        global = (int) (0.21 * R + 0.72 + G + 0.07 *B);
                        break;
                }
                // we set the new bitmap
                bmOut.setPixel(x, y, Color.argb(A, global, global, global));
            }
        }

        return bmOut;
    }

    public ImageManager setImage(ImageView image){
        this.srcBitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
        return this;
    }
}
