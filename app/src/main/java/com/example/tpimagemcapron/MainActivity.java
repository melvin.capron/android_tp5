package com.example.tpimagemcapron;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private final int PICK_IMAGE = 0;
    private boolean init = false;
    private Bitmap initialBitmap;
    private ImageManager imageManager = new ImageManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init context menu
        registerForContextMenu(getImageView());

        //Init load image button
        Button clickButton = (Button) findViewById(R.id.loadImage);
        clickButton.setOnClickListener(buttonCallback());

        //Init restore button
        Button restoreButton = (Button) findViewById(R.id.restore);
        restoreButton.setOnClickListener(restoreCallback());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(menu, v, contextMenuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Bitmap bitmapOut;
        this.imageManager.setImage(this.getImageView());
//Context menu of imageview
        switch (item.getItemId()) {
            case R.id.invertColors:
                bitmapOut = this.imageManager.invertColors();
                setImageView(bitmapOut);
                return true;
            case R.id.greyColors:
                bitmapOut = this.imageManager.toGrayLevel(1);
                setImageView(bitmapOut);
                return true;
            case R.id.greyColorsMaxRvb:
                bitmapOut = this.imageManager.toGrayLevel(2);
                setImageView(bitmapOut);
                return true;
            case R.id.greyFixColors:
                bitmapOut = this.imageManager.toGrayLevel(3);
                setImageView(bitmapOut);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //On menu flipping
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menu.setGroupVisible(R.id.groupMenuFlip, false);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (init) {
            menu.setGroupVisible(R.id.groupMenuFlip, true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    private ImageView getImageView() {
        return (ImageView) findViewById(R.id.loadedImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bitmap bitmapOut;
        this.imageManager.setImage(this.getImageView());

        //All possible flips
        switch (item.getItemId()) {
            case R.id.flipHorizontal:
                bitmapOut = this.imageManager.mirrorImageHorizontal();
                setImageView(bitmapOut);
                return true;
            case R.id.flipVertical:
                bitmapOut = this.imageManager.mirrorImageVertical();
                setImageView(bitmapOut);
                return true;
            case R.id.clockwise:
                bitmapOut = this.imageManager.clockwise();
                setImageView(bitmapOut);
                return true;
            case R.id.anticlockwise:
                bitmapOut = this.imageManager.anticlockwise();
                setImageView(bitmapOut);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setImageView(Bitmap bitmapIn) {
        getImageView().setImageBitmap(bitmapIn);
    }

    private View.OnClickListener restoreCallback() {
        //callback to reset the image
        return v -> {
            setImageView(initialBitmap);
        };
    }

    private View.OnClickListener buttonCallback() {
        return v -> {
            //Init the action get picture intent
            Intent imagePicker = new Intent(Intent.ACTION_GET_CONTENT);
            imagePicker.setType("image/*");
            startActivityForResult(Intent.createChooser(imagePicker, "Select a picture please"), PICK_IMAGE);
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri imagePath = data.getData();

            try {
                //Loads the image stream...
                InputStream imageStream = getContentResolver().openInputStream(imagePath);

                //Setting the options
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inMutable = true;

                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream, null, options);
                this.init = true;
                //Append to element the data given by the activity result
                setImageView(selectedImage);

                ((TextView) findViewById(R.id.loadedPathImage)).setText(imagePath.toString());
                initialBitmap = selectedImage;
                invalidateOptionsMenu();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}